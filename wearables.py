#!/usr/bin/env python

import os
import socket
import sys
import time
import struct
import fcntl
import errno

class wearables_server_t(object):
    def __init__(self,debug=False,mcaddr='239.255.223.01',port=0xDF0D,demo=False):
        self.debug = debug
        self.mcaddr = mcaddr
        self.port = port
        self.demo = demo
        self.cnt_send = 0

        # Disable wearables server for now (e.g. joystick protocol) until details of
        # participation in FISHLIGHT multi-cast are worked out.
        self.network_status = False
        return
    
        self.network_status = True
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.s.bind((self.mcaddr, self.port))
            mreq = struct.pack("4sl", socket.inet_aton(self.mcaddr), socket.INADDR_ANY)
            self.s.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        except:
            self.network_status = False
        
    def send_msg(self,msg):
        if not self.network_status:
            return
        
        MESSAGE = 0xDF0002
        elapsed = beat = hue_med = hue_dev = 0
        if type(msg) is tuple:
            msg_effect = msg[0]
            if msg[1] in ['fast']:
                hue_med = 2
            elif msg[1] in ['slow']:
                hue_med = 1
            else:
                pass
            if msg[0] in ['eye_goto']:
                elapsed = msg[2]
                beat = msg[3]
        elif type(msg) is list:
            raise
        else:
            msg_effect = msg

        if self.debug:
            print ("TX   %-16s elapsed: %04d beat: %04d hue_med:%03d hue_dev:%03d" % (msg_effect, elapsed, beat, hue_med, hue_dev))
        frame = struct.pack("!I12s16sIIBB", MESSAGE, '', msg_effect, elapsed, beat, hue_med, hue_dev)
        self.s.sendto(frame, (self.mcaddr, self.port))
        self.cnt_send += 1
        if self.debug:
            print ('cnt_send: {}'.format(self.cnt_send))
        
        
class wearables_client_t(object):
    def __init__(self,debug=False,mcaddr='239.255.223.01',mcport=0xDF0D,demo=False):
        self.debug = debug
        self.mcaddr = mcaddr
        self.mcport = mcport
        self.demo = demo
        self.cnt_recv = 0
        self.network_status = True
        self.PATTERN_DURATION_msec = 10000
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        try:
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except AttributeError:
            print ('*** Init failed (0) -- Wearables client ***')            
            pass

        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32) 
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
        self.sock.bind((self.mcaddr, self.mcport))
        #host = socket.gethostbyname(socket.gethostname())
        host = self.get_ip()
        print ('host: {}'.format(host))
        self.sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(host))
        self.sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, 
                             socket.inet_aton(self.mcaddr) + socket.inet_aton(host))
#       except:
#            print ('*** Init failed (1) -- Wearables client ***')
#            self.network_status = False

#        raise
            
        #mreq = struct.pack("4sl", socket.inet_aton(self.mcaddr), socket.INADDR_ANY)
        #self.s.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        if not self.demo and self.network_status:
            try:
                fcntl.fcntl(self.sock,fcntl.F_SETFL, os.O_NONBLOCK)
                pass
            except:
                print ('*** Init failed (2) -- Wearables client ***')                
                self.network_status = False

    def patternbit(self,pattern,bitNum):
        sizeofPatternBits = 4 # 4 bytes (32-bit encoding)
        return (pattern & (1 << (sizeofPatternBits * 8 - bitNum)) != 0)
    
    def paletteNameFromPattern(self,pattern):
        firstPaletteBit = 17
        
        paletteName = None
        if self.patternbit(pattern, firstPaletteBit): # nature
            if self.patternbit(pattern, firstPaletteBit + 1): # rainbow
                paletteName = 'OCPrainbow'
            else: #frolick
                if self.patternbit(pattern, firstPaletteBit + 2): # forest
                    paletteName = 'OCPforest'
                else: # party
                    paletteName = 'OCPparty'
        else: # hot&cold
            if self.patternbit(pattern, firstPaletteBit + 1): # cold
                if self.patternbit(pattern, firstPaletteBit + 2): # cloud
                    paletteName = 'OCPcloud'
                else: #ocean
                    paletteName = 'OCPocean'
            else: # hot
                if self.patternbit(pattern, firstPaletteBit + 2): # lava
                    paletteName = 'OCPlava'
                else: # heat
                    paletteName = 'OCPheat'

        print ('paletteName: {}'.format(paletteName))
        
        return paletteName
        
    def patternNameFromBits(self,pattern):
        patternName = 'UNKNOWN'
        
        if self.patternbit(pattern, 1):       # 1x - palette
            if self.patternbit(pattern, 2):     # 11x - palette waves
                if self.patternbit(pattern, 3): # 111x - spin
                    patternName = 'spin_pattern'
                else: # 110x - hiphotic
                    patternName = 'hipnotic_pattern'
            else: # 10x - palette balls
                if self.patternbit(pattern, 3): # 101x - metaballs
                    patternName = 'metaballs_pattern'
                else: # 100x - colored bursts
                    patternName = 'colored_bursts_pattern'
        else: # 0x - custom
            if self.patternbit(pattern, 2): # 01x - sparkly
                if self.patternbit(pattern, 3): # 011x - flame
                    patternName = 'flame_pattern'
                else: # 010x - glitter
                    patternName = 'glitter_pattern'
            else: # 00x - shiny
                if self.patternbit(pattern, 3): # 001x - threesine & the-matrix
                    if self.patternbit(pattern, 4): # // 0011x - the-matrix
                        patternName= 'thematrix_pattern'
                    else: # 0010x - threesine
                        patternName = 'threesine_pattern'
                else: # 00x - rainbow
                    patternName = 'rainbow_pattern'

        return patternName
    
    def get_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(0)        
        try:
            # doesn't even have to be reachable
            s.connect(('10.254.254.254', 1))
            IP = s.getsockname()[0]
        except Exception:
            IP = '127.0.0.1'
        finally:
            s.close()
        return IP
    
    def get_msgs_nonblocking(self):
        if not self.network_status:
            return None
        
        msgs = None
        while True:
            try:
                data, addr = self.sock.recvfrom(1024)
                #print ("***************** Received multicast data *******************")
            except socket.error as e:
                if e.args[0] == errno.EWOULDBLOCK:
                    #print ("*** WOULDBLOCK ***")
                    break
                elif e.args[0] == errno.EGAIN:
                    #print ("*** EGAIN ***")                    
                    break
            except Exception as err:
                print ('Error: {}'.format(err))
                #print "RX %d bytes, %s" % (len(data), err)
                raise

            try:
                # Decode the message as per:
                # 1 byte set to 0x10
                # 6 byte originator MAC address
                # 6 byte sender MAC address
                # 2 byte precedence
                # 1 byte number of hops
                # 2 bytes time delta since last origination
                # 4 byte current pattern
                # 4 byte next pattern
                # 2 byte time delta since start of current pattern
                
                (versionByte, thisDeviceMacAddress, thisDeviceMacAddress,
                 precedence, numHops, timeDeltaSinceOrigination,
                 currentPattern, nextPattern, timeDeltaSinceStartOfCurrentPattern) = \
                        struct.unpack('!B6s6sHBHIIH', data)

                x_pos,y_pos = None,None
                if False: # Disable legacy use of wearables protocol
                    effect = effect.rstrip('\x00')
                    if effect in ['eye_goto']:
                        x_pos = elapsed
                        y_poas = beat
                
                    if hue_med in [2]:
                        effect = (effect,'fast')
                    elif hue_med in [1]:
                        effect = (effect,'slow')
                    elif hue_med in [0]:
                        pass
                    else:
                        print ('** Unexpected hue_med encoding: {}'.format(elapsed))

                    if x_pos is not None and y_pos is not None:
                        effect = (effect[0], effect[1], x_pos,y_pos)
                    
                msg_rec = {
                    'versionByte': versionByte,
                    'deviceMacAddress': thisDeviceMacAddress,
                    'precedence': precedence,
                    'numHops': numHops,
                    'timeDeltaSinceOrigination': timeDeltaSinceOrigination,
                    'currentPattern': currentPattern,
                    'nextPattern': nextPattern,
                    'timeDeltaSinceStartOfCurrentPattern' : timeDeltaSinceStartOfCurrentPattern
                    }
                if msgs is None:
                    msgs = []

                msgs.append(msg_rec)
                self.cnt_recv += 1
                if self.debug:
                    print ('cnt_recv: {}'.format(self.cnt_recv))
                
                if self.debug:
                    print ("RX %s:%s   %-16s elapsed: %04d beat: %04d hue_med: %03d hue_dev: %03d" % (addr[0], addr[1], effect, elapsed, beat, hue_med, hue_dev))
#                    print "RX %s:%s   %-16s elapsed: %04d beat: %04d hue_med: %03d hue_dev: %03d" % (addr[0], addr[1], effect.rstrip('\0'), elapsed, beat, hue_med, hue_dev)
            except socket.error as e:
                if e.args[0] == errno.EWOULDBLOCK:
                    break
                elif e.args[0] == errno.EGAIN:
                    break
                else:
                    raise
            except Exception as err:
                print ('Error: {}'.format(err))
                #print "RX %d bytes, %s" % (len(data), err)
                break
            
        return msgs
    
if __name__ == "__main__":
    wearables_client = wearables_client_t(debug=False,demo=False)
    while True:
        msg = wearables_client.get_msg_nonblocking()
        if msg is not None:
            print ('msg: {}'.format(msg))
    
